///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file catDatabase.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdbool.h>

//#include "catDatabase.h"

#define MAX_CAT           30
#define MAX_CHAR_CAT_NAME 30
#define ERROR             printf( "ERROR" );


enum catGender{ UNKNOWN_GENDER, MALE, FEMALE };
enum catBreed{ UNKNOW_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };

bool catIsFixed;
int currentCatListSize;

char  catNameList[MAX_CAT][MAX_CHAR_CAT_NAME];
int   genderList[MAX_CAT];
int   breedList[MAX_CAT];
bool  isFixedList[MAX_CAT];
float catWeightList[MAX_CAT];