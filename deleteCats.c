///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file deleteCats.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "deleteCats.h"


void deleteAllCats() {

    for (int i = 0; i < MAX_CAT; i++ ) {

        strncpy( catNameList[i], "", MAX_CHAR_CAT_NAME );
        genderList[i]    = 0;
        breedList[i]     = 0;
        isFixedList[i]   = 0;
        catWeightList[i] = 0;

    }

    currentCatListSize   = 0; 

}

int deleteCat( int index ) {

    if ( index < 0 || index > (currentCatListSize - 1) || index > MAX_CAT ) {

        return printf( "No cat found at index = [%d]\n", index );

    }

    for ( int i = index; i < currentCatListSize; i++ ) {

        strncpy( catNameList[i], catNameList[ i + 1 ], MAX_CHAR_CAT_NAME );
        genderList[i]    = genderList[ i + 1 ];
        breedList[i]     = breedList[ i + 1 ];
        isFixedList[i]   = isFixedList[i + 1 ];
        catWeightList[i] = catWeightList[i + 1 ];

    }

    currentCatListSize--;

    return currentCatListSize;

}