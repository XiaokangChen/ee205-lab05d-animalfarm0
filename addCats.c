///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file addCats.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "addCats.h"


int addCat( char name[], int gender, int breed, bool isFixed, float weight) {
    
    if ( currentCatListSize > MAX_CAT ) {

        ERROR
        return -1;
        
    }

    if ( strlen(name) <= 0 || strlen(name) > MAX_CHAR_CAT_NAME ) {

        ERROR
        return -1;

    }

    for ( int i = 0; i < currentCatListSize; i++ ) {

        if ( strcmp(catNameList[i], name) == 0 ) {

            ERROR
            return -1;

        }
        
    }

    if ( weight <= 0 ) {

        ERROR
        return -1;

    }

    strncpy( catNameList[currentCatListSize], name, MAX_CHAR_CAT_NAME );
    genderList[currentCatListSize]    = gender;
    breedList[currentCatListSize]     = breed;
    isFixedList[currentCatListSize]   = isFixed;
    catWeightList[currentCatListSize] = weight;
    
    currentCatListSize++;


    return currentCatListSize;

}
