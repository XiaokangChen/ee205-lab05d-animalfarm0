///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file main.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

#include "catDatabase.h"
#include "addCats.h"
#include "reportCats.h"
#include "updateCats.h"
#include "deleteCats.h"

#define NL printf("\n");


void checkListSize() {

    for ( int i = 0; i < MAX_CAT; i++ ) {

        if ( catWeightList[i] != 0 ) {
            
            currentCatListSize++;

        }

    }
    
}

int main(void) {

    deleteAllCats();
    checkListSize();

    addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;                     //index = 0
    addCat( "Milo", MALE, MANX, true, 7.0 ) ;                        //index = 1
    addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;              //index = 2
    addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;                //index = 3
    addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;                     //index = 4
    addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;      //index = 5
    addCat( "" , UNKNOWN_GENDER, UNKNOW_BREED, false, 1 );           //Fail
    addCat( "TestCat" , UNKNOWN_GENDER, UNKNOW_BREED, false, -100 ); //Fail

    printAllCats();
    NL
    int kali = findCat( "Kali" ) ;
    updateCatName( kali, "Chili" ) ; // this should fail
    printCat( kali );
    updateCatName( kali, "Capulet" ) ;
    updateCatWeight( kali, 9.9 ) ;
    fixCat( kali ) ;
    printCat( kali );
    NL
    printAllCats();
    NL
    deleteCat(0);
    printAllCats();
    NL
    printAllCats();
	
	
    //for testing purpose
    //printf( "cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", 0, catNameList[0], genderList[0], breedList[0], isFixedList[0], catWeightList[0] );

}
