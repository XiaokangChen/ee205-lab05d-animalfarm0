///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file reportCats.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "addCats.h"

int printCat( int index ) {

    if ( index < 0 || index > (currentCatListSize - 1) || index > MAX_CAT) {

        return printf( "Bad Cat [%d]\n", index );

    }

    return printf( "cat index = [%d] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", index, catNameList[index], genderList[index], breedList[index], isFixedList[index], catWeightList[index] );

}

void printAllCats() {

    if ( currentCatListSize == 0 ) {

        printf( "You have no cat in your database, go add some cats!!!\n" );
    }

    for ( int i = 0; i < currentCatListSize; i++ ) {

        printCat(i);

    }

}

int findCat( char name[] ) {

    for ( int i = 0; i < currentCatListSize; i++ ) {

        if ( strcmp(catNameList[i], name) == 0 ) {

            printf( "[%s] is at Index = [%d]\n", name, i );
            return i;

        }

    }

    ERROR
    return printf( "[%s] not found\n", name );

}
