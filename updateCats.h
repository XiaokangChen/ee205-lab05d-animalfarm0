///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file updateCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#ifndef UPDATECATS_H_
#define UPDATECATS_H_


extern int updateCatName( int index, char NewName[] );
extern int fixCat( int index );
extern int updateCatWeight( int index, float newWeight );


#endif
