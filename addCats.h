///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file addCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#ifndef ADDCATS_H_
#define ADDCATS_H_


extern int addCat( char name[], int gender, int breed, bool isFixed, float weight);


#endif
