///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file reportCats.h
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#ifndef REPORTCATS_H_
#define REPORTCATS_H_


extern int printCat( int index );
extern void printAllCats();
extern int findCat( char name[] );


#endif
