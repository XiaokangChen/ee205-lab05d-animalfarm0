///////////////////////////////////////////////////////////////////////////////
///////        University of Hawaii, College of Engineering
/////// @brief Lab 05d - animalFarm0 - EE 205 - Spr 2022
///////
/////// @file updateCats.c
/////// @version 1.0
///////
/////// @author Xiaokang Chen <xiaokang@hawaii.edu>
/////// @date 10 Feb 2022
///////////////////////////////////////////////////////////////////////////////


#include "catDatabase.h"
#include "updateCats.h"


int updateCatName( int index, char NewName[] ) {

    if ( index < 0 || index > (currentCatListSize - 1) || index > MAX_CAT ) {

        return ERROR

    }

    if ( strlen(NewName) <=0 || strlen(NewName) > MAX_CHAR_CAT_NAME ) {

        return ERROR

    }

    for ( int i = 0; i < currentCatListSize; i++ ) {

        if ( strcmp(catNameList[i], NewName) == 0 ) {

            return ERROR

        }

    }

    strncpy( catNameList[index], NewName, MAX_CHAR_CAT_NAME );

    return printf( "Your cat name has been Updated to [%s]\n", catNameList[index] );

}

int fixCat( int index ) {

    if ( index < 0 || index > (currentCatListSize - 1) || index > MAX_CAT ) {

        return ERROR

    }

    isFixedList[index] = true;

    return printf( "[%s] has been fixed\n", catNameList[index] );

}

int updateCatWeight( int index, float newWeight ) {

    if ( index < 0 || index > (currentCatListSize - 1) || index > MAX_CAT ) {

        return ERROR

    }

    if ( newWeight <= 0 ) {

        return ERROR

    }

    catWeightList[index] = newWeight;

    return printf( "[%s] now weight [%f]\n", catNameList[index], catWeightList[index] );

}
